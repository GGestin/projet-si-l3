const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
//const mysql = require('mysql');

const indexRouter = require('./routes/index');
const connexionRouter = require('./routes/connexion');
const inscriptionRouter = require('./routes/inscription');

const app = express();


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/connexion',connexionRouter);
app.use('/inscription',inscriptionRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});


// TEST CONNEXION COURS : 

/*
const connection = mysql.createConnection({
  host : 'obiwan2.univ-brest.fr',
  user : 'zvadeleal',
  password : '2bxys5xb',
  database : 'zil3-zvadeleal',
});

connection.connect();


connection.query('SELECT 5 AS valeur', function(error, results, fields){
  if(error) throw error;
  console.log('le resultat de la requête est : ',results[0].valeur);

});
connection.end();
*/


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
