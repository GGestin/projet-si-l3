import React, { Component } from 'react';
import './App.css';
import Header from './components/layout/Header';
import Connexion from './components/Connexion';
import Inscription from './components/Inscription';

class App extends Component {

  render(){
    return (
      <div className="App">
        <header className="App-header">
         
        <Header/>
        {/*<Connexion/>
        */}
        <Inscription/>

        </header>
       
         
      </div>
    );

  }

  
}

export default App;
