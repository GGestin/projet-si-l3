import React, { Component } from 'react'

export class Inscription extends Component {
    state = {
        nom:'',
        prenom:'',
        parcours:'',
        telephone:'',
        dateDeNaissance:'',
        nationalite:'',
        emailUBO:'',
        motDePasse:''
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onSubmit = (e) =>{
        e.preventDefault();
    }

    render() {
        return (
            <form onSubmit={this.onSubmit}>
                <p>Pour accéder à l'application, veuillez vous inscrire : </p>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <input 
                            type="text"
                            class="form-control" 
                            name="nom"
                            placeholder = " Nom..."
                            onChange={this.onChange}
                            
                        />
                    </div>
                    <div class="form-group col-md-6">
                    <input 
                        type="text" 
                        class="form-control" 
                        name="prenom"
                        placeholder="Prénom..."
                        onChange={this.onChange}
                        />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <input 
                            type="text" 
                            class="form-control" 
                            name="parcours"
                            placeholder="Parcours ..."
                            onChange={this.onChange}
                            />
                    </div>
                    <div class="form-group col-md-6">
                        <input
                            type="text" 
                            class="form-control" 
                            name="telephone" 
                            placeholder="Téléphone ..."
                            onChange={this.onChange}
                        />
                    </div>
                </div>    
                <div class="form-row">

                    <div class="form-group col-md-6">
                        <input
                            type="date" 
                            class="form-control" 
                            name="dateDeNaissance" 
                            placeholder="Téléphone ..."
                            onChange={this.onChange}
                        />
                    </div>

                    
                    <div class="form-group col-md-6">
                    <select name="nationalite" class="form-control" onChange={this.onChange} >
                        <option disabled selected>Nationalité :</option>
                        <option>Française</option>
                        <option>Autre</option>
                    </select>
                    </div>
                </div>

                <div class="form-group">
                        <input
                            type="email" 
                            class="form-control" 
                            name="emailUBO" 
                            placeholder="Adresse mail de l'UBO ..."
                            onChange={this.onChange}
                        />
                    </div>
                    <div class="form-group">
                        <input
                            type="password" 
                            class="form-control" 
                            name="motDePasse" 
                            placeholder="Mot de passe ..."
                            onChange={this.onChange}
                        />
                    </div>

                <div class="form-group">
                    <div class="form-check">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Sign in</button>
            </form>
            
        )
    }
}

export default Inscription;
