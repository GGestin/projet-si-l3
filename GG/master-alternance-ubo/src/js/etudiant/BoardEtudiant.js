import React from 'react';
import "./BoardEtudiant.css";
import Liste from '../components/Liste';
import ListeEntretientsUBO from '../administrateur/enseignant/ListeEntretientsUBO'

import Menu from '../components/Menu';

class BoardEtudiant extends React.Component {
    render() {
        const list = [
            {
                firstname: 'BONJOUR',
                lastname: 'Wieruch',
                email: 'robinbin@BigInt.bin',
            },
            {
                firstname: 'Dave',
                lastname: 'Davidds',
                email: 'DAVIDTENANTOMGTOBIEN.fr',
            },
            {
                firstname: 'David',
                lastname: 'Doodle',
                email: 'djfuhzrguzgzref@gmail.com',
            },
            {
                firstname: 'Devan',
                lastname: 'TheBest',
                email: 'omgDavidMeManque.com',
            },
            {
                firstname: 'Denmark',
                lastname: 'dead',
                email: 'NeMePosePasLaQuestion.qsd',
            },
        ];
        console.log(this.props.datas);
        return (
            <div id="container">
                <Menu/>
                <div className="BoardHeader">
                    <h1>Bienvenue </h1>
                    <h1 id="headerSurname">{this.props.datas.prenomEtudiant}</h1>
                    <h1 id="headerName">{this.props.datas.nomEtudiant}</h1>
                </div>
                <table className="infos text-left m-3">
                    <tbody>
                        <tr> 
                            <td><strong>Nom :</strong></td> 
                            <td><p id="name">{this.props.datas.nomEtudiant}</p></td>
                        </tr>
                        <tr>
                            <td><strong>Prénom :</strong></td>
                            <td><p id="surname">{this.props.datas.prenomEtudiant}</p></td>
                        </tr>
                        <tr>
                            <td><strong>Mail :</strong></td>
                            <td><p id="mail">{this.props.datas.prenomEtudiant}.{this.props.datas.nomEtudiant}@mail.com</p></td>
                        </tr>
                        <tr>
                            <td><strong>Parcours actuel:</strong></td>
                            <td><p id="actualParcours"> {this.props.datas.parcours}</p></td>
                        </tr>
                        <tr>
                            <td><strong>Parcours visé:</strong></td>
                            <td><input type="dropdown" className="form-control" id="targetParcours" defaultValue="Parcours..."></input></td>
                        </tr>
                        <tr>
                            <td><strong>CV :</strong></td>
                            <td>
                                <div className="custom-file ">
                                    <input type="file" className="custom-file-input" id="customFile"/>
                                    <label className="custom-file-label" htmlFor="customFile">Choisir le CV...</label>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table className="table">
                    <tbody><tr>
                        <ListeEntretientsUBO/>
                    </tr></tbody>
                </table>
            </div>
        );
    }
}

export default BoardEtudiant;