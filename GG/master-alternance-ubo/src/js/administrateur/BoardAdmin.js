import React from 'react';
import ListeEntretientsUBO from './enseignant/ListeEntretientsUBO'
import Menu from '../components/Menu';

class BoardAdmin extends React.Component {
    render() {
        return (
            <div id='container'>
                <Menu/>
                BoardAdmin
                <ListeEntretientsUBO/>
            </div>
        )
    }
}

export default BoardAdmin;

