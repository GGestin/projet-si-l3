import React from 'react';
import Liste from '../../components/Liste'
import $ from 'jquery';

class ListeEntretientsUBO extends React.Component {
    constructor(props) {
        super(props);
        this.search = this.search.bind(this);
    }

    search(event) {
        var value = $("#myInput").val().toLowerCase();
        // eslint-disable-next-line
        $("#myTable tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    };

    render() {
        const list = [
            {
                firstname: 'Robin',
                lastname: 'Wieruch',
                email: 'robinbin@BigInt.bin',
            },
            {
                firstname: 'Dave',
                lastname: 'Davidds',
                email: 'DAVIDTENANTOMGTOBIEN.fr',
            },
            {
                firstname: 'David',
                lastname: 'Doodle',
                email: 'djfuhzrguzgzref@gmail.com',
            },
            {
                firstname: 'Devan',
                lastname: 'TheBest',
                email: 'omgDavidMeManque.com',
            },
            {
                firstname: 'Denmark',
                lastname: 'dead',
                email: 'NeMePosePasLaQuestion.qsd',
            },
        ];

        return (
            <div className="h-100">
                <p className="display-4">Liste des entretiens organisés par l'UBO</p>
                <div className="Searchfield pb-4">
                    <label htmlFor="pwd">Recherche : </label>
                    <input id="myInput" type="text" className="form-control" placeholder="Nom..." onKeyUp={this.search}/>
                </div>
                <Liste list={list} class="col" />
            </div>
        );
    }
}

export default ListeEntretientsUBO;