import React from 'react';

class Liste extends React.Component {
    render() {
        let i = 0;
        return (
            <div className="TableContainer">
                <table className="table table-hover table-striped">
                    <thead className="thead-light">
                        <tr>
                            <th>Firstname</th>
                            <th>Lastname</th>
                            <th>Email</th>
                        </tr>
                    </thead>
                    <tbody id="myTable">
                        {this.props.list.map(item => (
                            <tr key={i++}>
                                <td key={item.firstname}>
                                    {item.firstname}
                                </td>
                                <td key={item.lastname}>
                                    {item.lastname}
                                </td>
                                <td key={item.email}>
                                    {item.email}
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Liste;