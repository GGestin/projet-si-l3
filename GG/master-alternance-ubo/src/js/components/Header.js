import React from 'react';

class Header extends React.Component {
    debugLogin = () => {
        this.props.onLoginChange("student");
    }

    debugLogout = () => {
        this.props.onLoginChange(undefined);
    }

    render() {
        return (
            <header className="Header d-flex">
                <img src="./images/logo.png" alt="Home page"/>
                <h3 className="pl-3 align-self-center mr-auto">Master Alternance</h3>
                {this.props.login &&
                <div className="d-flex">
                    <h4 id="1stNameHeader" className="align-self-center p-1">{this.props.datas.prenomEtudiant}</h4>
                    <h4 id="2ndNameHeader" className="align-self-center p-1">{this.props.datas.nomEtudiant}</h4>
                    <input className="p-2 buttonHeader" type="button" value="Déconnexion" onClick={this.debugLogout}/>
                </div>
                }
                {!this.props.login &&
                <div className="d-flex">
                    <input className="p-2 buttonHeader" type="button" value="Connexion" onClick={this.debugLogin}/>
                </div>
                }
            </header>
        );
    }
}

export default Header;