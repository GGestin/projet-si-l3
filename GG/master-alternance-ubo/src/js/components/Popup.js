import React from "react";

class Popup extends React.Component {
    render() {
        return (
            <div className="popup-box">
                <div className="box">
                    <span className="close-icon" onClick={this.props.handleClose}>x</span>
                    <h2>{this.props.title}</h2>
                    <pre>{this.props.content}</pre>
                </div>
            </div>
        );
    }
};

export default Popup;