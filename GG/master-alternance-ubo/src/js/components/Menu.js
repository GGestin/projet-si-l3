import React from 'react';

class Menu extends React.Component {
    goTo = (param) => {
        this.props.onMenuChange(param);
    }

    render() {
        return (
            <div className="d-flex justify-content-start bg-secondary mb-3">
                {this.props.role === "admin" &&
                    <input type="button" value="Home" onClick={this.goTo("AdminConsole")}/>
                }
            </div>
        );
    }
}

export default Menu;