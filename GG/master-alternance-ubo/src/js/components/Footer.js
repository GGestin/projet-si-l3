import React from 'react';

class Footer extends React.Component {
    render() {
        return (
            <footer className="d-flex justify-content-between mt-auto mb-0 p-3">
                <p className="align-self-center">Master Alternance UBO 2020</p>
                <p className="align-self-center">2020@Université Bretagne Sud</p>
                <p className="align-self-center">GGestin</p>
            </footer>
        );
    }
}

export default Footer;