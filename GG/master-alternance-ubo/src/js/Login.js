import React from 'react';
import $ from 'jquery';

class Login extends React.Component {
    logMeIn = (event) => {
        let me = this;
        let id = $("#idInput")[0].value;
        let ps = $("#psInput")[0].value;
        if (id && ps) {
            let adr = "http://localhost:" + process.env.REACT_APP_API_PORT;
            $.getJSON(adr + "/login", {
                id: id,
                ps: ps
            }).done((data) => {
                if (data.length) {
                    let rl = data[0].usersRole
                    $.getJSON(adr + "/info", {
                        id: id,
                        rl: rl
                    }).done((data) => {
                        if (data.length) {
                            me.props.onDataReceived(data[0]);
                            me.props.onLoginChange(rl);
                        }
                    });
                } else
                    me.props.onPop("Identifiant ET/OU Mot de passe incorrect.", "Erreur Incorrect");
            });
        }
        else
        {
            me.props.onPop("Identifiant ET/OU Mot de passe vide.\nMerci de remplir les champs nécessaires", "Erreur Vide");
        }
    }

    render() {
        return (
            <div className="Login">
                <h1 className="display-4">Bienvenue sur le site de gestion des alternances Master à l'UBO</h1>
                <hr/>
                <h2 className="my-4">Connectez-vous</h2>
                <div className="row Form">
                    <div className="input-group mb-3">
                        <div className="input-group-prepend">
                            <span className="input-group-text">Identifiant</span>
                        </div>
                        <input id="idInput" type="text" className="form-control" placeholder="Identifiant/email..."/>
                    </div>
                    <div className="input-group mb-3">
                        <div className="input-group-prepend">
                            <span className="input-group-text">Mot de Passe</span>
                        </div>
                        <input id="psInput" type="password" className="form-control" placeholder="Mot de passe..."/>
                    </div>
                </div>
                <input className="btn btn-primary btn-lg" type="button" value="Connection" onClick={this.logMeIn}/>
            </div>
        );
    }
}

export default Login;