import React from 'react';
import './App.css';

import Header from './js/components/Header';
import Footer from './js/components/Footer';
import Login from './js/Login';
import Popup from "./js/components/Popup";
import BoardEtudiant from "./js/etudiant/BoardEtudiant";
import BoardEnseignant from "./js/administrateur/enseignant/BoardEnseignant";
import BoardAdmin from './js/administrateur/BoardAdmin';

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			connected: false,
			role: "Tenant",
			page: null,
			isOpen: false,
			popContent: null,
			popTitle: null,
			datas: null
		}
	}

	togglePopup = (content, title) => {
		this.setState({ isOpen: !this.state.isOpen, popContent: content, popTitle: title });
	}

	handleConnection = (role) => {
		this.setState({ connected: role !== undefined, role: role });
	}

	handleData = (data) => {
		this.setState({datas: data});
	}

	render() {
		var page;
		if (this.state.connected) {
			// eslint-disable-next-line
			switch (this.state.role) {
				case "student":
					page = <BoardEtudiant datas={this.state.datas}/>
					break;
				case "admin":
					page = <BoardAdmin datas={this.state.datas}/>
					break;
				case "teacher":
					page = <BoardEnseignant datas={this.state.datas}/>
					break;
			}
		}
		return (
			<div className="App">
				{this.state.isOpen && <Popup content={this.state.popContent} title={this.state.popTitle} handleClose={this.togglePopup} />}
				<Header login={this.state.connected} onLoginChange={this.handleConnection} datas={this.state.datas}/>
				{!this.state.connected && <Login onPop={this.togglePopup} onLoginChange={this.handleConnection} onDataReceived={this.handleData}/>}
				{page}
				<Footer />
			</div>
		);
	}
}

export default App;
