const express = require('express');
const querystring = require('querystring');
const http = require('http');
const url = require('url');
const cors = require('cors');
const app = express();
const router = express.Router();
require('dotenv').config();

router.use((req, res, next) => {
	console.log('Time:', Date.now());
	next();
});

router.get('/login', (req, res) => {
	res.set('Access-Control-Allow-Origin', '*');
	var thePath = "/login?id=" + req.query.id + "&ps=" + req.query.ps;
	let options = {
		hostname: 'localhost',
		port: process.env.DB_PORT,
		path: thePath,
		method: 'GET',
	}
	const request = http.request(options, (result) => {
		// Get every chunk of data
		let json = '';
		result.on('data', function (chunk) {
			json += chunk;
		});
	
		result.on('end', () => {
			console.log('Status:', result.statusCode);
			if ((result.statusCode === 200) && (json)) {
				try {
					var data = JSON.parse(json);
					res.status(200).send(data);
				} catch (e) {
					console.log('Error parsing JSON!');
					res.status(500).send(null);
				}
			} else if (result.statusCode === 200) {
				res.status(200).send({data: null});
			} else {
				console.log('Error:', result);
			}
		});
	});

	request.on('error', function (err) {
		console.log('Error:', err);
	})

	request.end();
});

router.get('/info', (req, res) => {
	res.set('Access-Control-Allow-Origin', '*');
	var thePath = "/info?id=" + req.query.id + "&rl=" + req.query.rl;
	let options = {
		hostname: 'localhost',
		port: process.env.DB_PORT,
		path: thePath,
		method: 'GET',
	}
	const request = http.request(options, (result) => {
		// Get every chunk of data
		let json = '';
		result.on('data', function (chunk) {
			json += chunk;
		});
	
		result.on('end', () => {
			console.log('Status:', result.statusCode);
			if ((result.statusCode === 200) && (json)) {
				try {
					var data = JSON.parse(json);
					res.status(200).send(data);
				} catch (e) {
					console.log('Error parsing JSON!');
					res.status(500).send(null);
				}
			} else if (result.statusCode === 200) {
				res.status(200).send({data: null});
			} else {
				console.log('Error:', result);
			}
		});
	});

	request.on('error', function (err) {
		console.log('Error:', err);
	})

	request.end();
});

app.use('/', router);

app.use(cors());

app.use((err, req, res, next) => {
	res.set('Access-Control-Allow-Origin', '*');
});

app.listen(process.env.REACT_APP_API_PORT || 7135);
console.log('API is listening at port ' + (process.env.REACT_APP_API_PORT || 7135));
