const mysql = require('mysql');
const express = require('express');
const cors = require('cors');
const app = express();
const router = express.Router();
require('dotenv').config();

try {
    var sql_pool = mysql.createPool({
        connectionLimit: 5,
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_DATABASE
    });
    console.log('SQL Connection Pool OK');
} catch (error) {
    console.error('SQL Connection Pool ERROR = ' + error);
}

router.use((req, res, next) => {
    console.log('Time:', Date.now());
    next();
});

router.get('/login', (req, res) => {
    res.set('Access-Control-Allow-Origin', '*');
    const { id, ps } = req.query;
    var columns = ['usersRole'];
    var query = sql_pool.query('SELECT ?? FROM USERS WHERE usersId = ? AND usersPs = ?;', [columns, id, ps], (err, resQuery) => {
        console.log(query.sql);
        console.log("Fetched: ", resQuery.length);
        // Case Error
        if (err) {
            console.log("Error: ", err);
            res.status(500).send(err);
        }
        // Case Ok
        res.status(200).send(resQuery);
    });
});

router.get('/info', (req, res) => {
    res.set('Access-Control-Allow-Origin', '*');
    const { id, rl } = req.query;
    var query = sql_pool.query('SELECT * FROM ?? WHERE idConnection = ?;', [rl, id], (err, resQuery) => {
        console.log(query.sql);
        // Case Error
        if (err) {
            console.log("Error: ", err);
            res.status(500).send(err);
        }
        // Case Ok
        res.status(200).send(resQuery);
    });
});

app.use('/', router);

app.use(cors());

app.listen(process.env.DB_PORT || 9135);
console.log('API is listening at port ' + (process.env.DB_PORT || 9135));
