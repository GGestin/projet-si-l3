# Tree du serveur react
/app
  - login.js
  - components/
      - header.js
      - popup.js
      - liste.js
      - graphs.js

  - etudiant/
      - board.js
  - enseignants/
      - board.js
      - entretUBO.js
      - entret.js
  - admin/
      - board.js
      - entretUBO.js
      - etudiants.js
      - admin.js

# API Appli
  - server.js
  - interBDD.js
  - auth.js

# API MySQL
  - server.js
  - modif.js
  - request.js
  - delete.js
  - insert.js
